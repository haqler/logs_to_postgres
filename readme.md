**Script for parsing log file**
---

Usage:

	parse_log.sh   --table-name your_table   --input-file log_file   --output-file script_output   --until-date your_date

Parameters:

	--table-name    -   Database table name. Mandatory.
	
	--input-file    -   Input file name. Mandatory.
	
	--output-file   -   Output file name. Optional. By default it's 'table_name_insert.sql'.
	
	--until-date    -   Process rows with date less than given. Date format: 'YYYY-MM-DD hh:mm:ss' or 'YYYYMMDDhhmmss'. 
	
	                    If not supplied tnen script will process all rows in the input file. (Date should be quoted)


Examples:
---

./parse_logs.sh --input-file log_file.txt --table-name sms

./parse_logs.sh --input-file log_file.txt --table-name sms --output-file some-name.sql

./parse_logs.sh --input-file log_file.txt --table-name sms --output-file some-name.sql --until-date '2018-06-13 22:47:23'

./parse_logs.sh --input-file log_file.txt --table-name sms --output-file some-name.sql --until-date '20180613224723'

