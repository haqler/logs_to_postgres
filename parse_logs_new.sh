#!/bin/bash


usage_print()
{
	# here document. Write text to output until stop word, in this case it's 'END'
	cat << "END"

Usage:
	parse_log.sh   --table-name your_table   --input-file log_file   --output-file script_output   --until-date your_date

Parameters:
	--table-name    -   Database table name. Mandatory.
	--input-file    -   Input file name. Mandatory.
	--output-file   -   Output file name. Optional. By default it's 'table_name_insert.sql'.
	--until-date    -   Process rows with date less than given. Date format: 'YYYY-MM-DD hh:mm:ss' or 'YYYYMMDDhhmmss'.
	                    If not supplied tnen script will process all rows in the input file.

END
}


# process user parameters
while [ "$#" -ne 0 ]; do
	case "$1" in
		'--table-name')
			TABLE_NAME="$2"
			shift
			shift
			;;

		'--input-file')
			INPUT_FILE="$2"
			shift
			shift
			;;

		'--output-file')
			OUTPUT_FILE="$2"
			shift
			shift
			;;

		'--until-date')
			UNTIL_DATE="$2"
			shift
			shift
			;;

		*)
			echo "ERR: unknown parameter '$1'"
			usage_print
			exit 1
			;;
	esac

done


if [ -z "$INPUT_FILE" ]; then
	echo "ERR: input file is not given"
	usage_print
	exit 1
fi

if ! [ -f "$INPUT_FILE" ]; then
	echo "ERR: '$INPUT_FILE' is not regular file"
	usage_print
	exit 1
fi

if [ -z "$TABLE_NAME" ]; then
	echo "ERR: Table name is not given"
	usage_print
	exit 1
fi


# default values
OUTPUT_FILE=${OUTPUT_FILE:=$TABLE_NAME'_insert.sql'}
if [ -n "$UNTIL_DATE" ]; then
	UNTIL_DATE=`sed 's/[-: ]//g' <<<$UNTIL_DATE`
	[[ "$UNTIL_DATE" =~ [^0-9] ]] && echo 'ERR: wrong date format' && usage_print && exit 1
fi

echo '========================='
echo 'Table name: '$TABLE_NAME
echo 'Until date: '$UNTIL_DATE
echo 'Input file: '$INPUT_FILE
echo 'Output file: '$OUTPUT_FILE
echo '========================='

if [ -f "$OUTPUT_FILE" ]; then
	echo "Removing $OUTPUT_FILE"
	rm "$OUTPUT_FILE"
fi


echo "INSERT INTO $TABLE_NAME (uuid, \"to\", from_id, send_time, msg) values " >>$OUTPUT_FILE

IFS=$'\n'
for i in $(awk '
	BEGIN {rfrom="from:"; rto="to:"; rmsg="msg:[0-9]+:\\[?"}
	$0 == "" {next}
	{date=$1" "$2}
	match($0, /from:\+[0-9]+/) {from_id=gensub(rfrom, "", 1, substr($0, RSTART, RLENGTH))}
	match($0, /to:\+[0-9]+/) {to_id=gensub(rto, "", 1, substr($0, RSTART, RLENGTH))}
	match($0, /msg:[^\]]*/) {msg=gensub(rmsg, "", 1, substr($0, RSTART, RLENGTH))}
	{printf "%s;%s;%s;%s\n",  date, from_id, to_id, msg}	
' $INPUT_FILE)
do
	UUID=`uuidgen -r`
	DATE=`cut -d ';' -f 1 <<<"$i"`
	FROM_ID=`cut -d ';' -f 2 <<<"$i"`
	TO_ID=`cut -d ';' -f 3 <<<"$i"`
	MSG=`cut -d ';' -f 4 <<<"$i"`

	if [ -n "$UNTIL_DATE" ]; then
		TMP_DATE=`sed 's/[-: ]//g' <<<$DATE`
		if [ "$TMP_DATE" -gt "$UNTIL_DATE" ]; then
			continue
		fi
	fi

	if [[ "$MSG" =~ ^[0-9A-F]+$ ]]; then
		# https://askubuntu.com/questions/848565/which-program-hex-editor-can-convert-from-hex-code-string-to-unicode
		#MSG=`echo "$MSG" | perl -CS -pe 's/[0-9A-F]{4}/chr(hex($&))/egi'`
		MSG=`echo "$MSG" | perl -pe 'binmode STDOUT, ":utf8";; use Encode qw/decode/; $_ = decode("UTF-16BE", pack("H*", $_));'`
	fi
	
	echo "('$UUID', '$TO_ID', '$FROM_ID', '$DATE', " '$qt$'$MSG'$qt$),' >>$OUTPUT_FILE
	#echo "INSERT INTO $TABLE_NAME (uuid, \"to\", from_id, send_time, msg) values ('$UUID','$TO_ID','$FROM_ID', '$DATE', " '$qt$'$MSG'$qt$);' >>$OUTPUT_FILE
done

last_line=$(wc -l $OUTPUT_FILE | cut -d ' ' -f 1)
sed -i -e "${last_line}s/,$/;/" $OUTPUT_FILE

echo 'FINISH'

exit 0

