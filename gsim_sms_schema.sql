--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.9
-- Dumped by pg_dump version 9.6.9

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: sms; Type: TABLE; Schema: public; Owner: webbackend
--

CREATE TABLE public.sms (
    uuid character varying(36) NOT NULL,
    "to" character varying(255) NOT NULL,
    from_id character varying(255),
    created_on timestamp with time zone DEFAULT now() NOT NULL,
    msg text,
    file character varying(255),
    code integer DEFAULT 0,
    send_time timestamp with time zone,
    user_id integer
);


ALTER TABLE public.sms OWNER TO vasya;

--
-- Name: sms sms_pkey; Type: CONSTRAINT; Schema: public; Owner: webbackend
--

ALTER TABLE ONLY public.sms
    ADD CONSTRAINT sms_pkey PRIMARY KEY (uuid);


--
-- Name: ix_sms_created_on; Type: INDEX; Schema: public; Owner: webbackend
--

CREATE INDEX ix_sms_created_on ON public.sms USING btree (created_on);


--
-- Name: ix_sms_from_id; Type: INDEX; Schema: public; Owner: webbackend
--

CREATE INDEX ix_sms_from_id ON public.sms USING btree (from_id);


--
-- Name: ix_sms_to; Type: INDEX; Schema: public; Owner: webbackend
--

CREATE INDEX ix_sms_to ON public.sms USING btree ("to");


--
-- Name: ix_sms_user_id; Type: INDEX; Schema: public; Owner: webbackend
--

CREATE INDEX ix_sms_user_id ON public.sms USING btree (user_id);


--
-- PostgreSQL database dump complete
--

