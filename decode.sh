#!/bin/bash


if [ -z "$1" ]; then
	echo "Give your message string"
	exit 1
fi


# https://askubuntu.com/questions/848565/which-program-hex-editor-can-convert-from-hex-code-string-to-unicode
res=$(echo "$1" | perl -CS -pe 's/[0-9A-F]{4}/chr(hex($&))/egi')
echo
echo $res
echo

exit 0

